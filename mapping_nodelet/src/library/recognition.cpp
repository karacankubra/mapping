#include <mapping_ros/library/recognition.h>
#include <cmath>
#define sind(x) (sin(fmod((x),360) * M_PI / 180))
#define cosd(x) (cos(fmod((x),360) * M_PI / 180))


using namespace std;


namespace mapping_nodelet
{
recognition::recognition()
{
	// rise requirement for the ramps
	widthReqVec << 0.02, 3.00;

	// Height distances
	ndFlag = true;
	nDistance << 0.07,0.25; //change it 0.24 to 0.4

	// Depth distances
	pdFlag = true;
	pDistance << 0.1,0.50; //change it from 0.18 to 0.11

}


void recognition::order( solution& output, pcl::PointCloud<pcl::PointXYZ> &floor, bool dir )
{

    solution result;
    segmentClouds temp;
    segmentClouds temp2;
    regions temp3;
    result.stairState = false; 
    result.objectState = false; 
    result.rampState = false; 
    std::vector<regions> objectCandidate;
    std::vector<int> usedLabel;

	float incSurfAngle;
    float incRise;
    float incWidth;
	float Hdistance;
	float Vdistance;
    float centerThirdTreadTransform;
    float minZ;
    float distanceXYObject;
    float distanceZObject;
    
    Eigen::Vector3f objectVector;
    Eigen::Vector3f initialDist;
    Eigen::Vector3f centerFirst;
    Eigen::Vector3f centerSecond;
    Eigen::Vector3f centerSecondRiser;
    Eigen::Vector3f centerFirstRiser;
    Eigen::Vector3f center;
    Eigen::Vector4f min;
    Eigen::Vector4f max;
    switch(dir)
    {
        case(true):
            {
                stairTreads.clear();
                incSurf.clear();
                stairRisers.clear();
                objectRegions.clear();
                object.clear();
//labeling
                for( int regCounter = 0; regCounter < stairRiseRegions.size(); regCounter++ )
                {
                    Vdistance = fabsf(stairRiseRegions.at(regCounter).height);
                    if ( Vdistance > nDistance[0] && Vdistance < nDistance[1])
                    {
                        result.stairRisers.push_back(stairRiseRegions.at(regCounter));
                        result.stairRisers.indic.push_back(stairRiseRegions.indic[regCounter]);
                    }
                }

                for( int regCounter = 0; regCounter < stairTreadRegions.size(); regCounter++ )
                {
                    Hdistance = fabsf(stairTreadRegions.at(regCounter).depth);
                    if ( Hdistance > pDistance [0] && Hdistance < pDistance[1] )
                    {
                        result.stairTreads.push_back(stairTreadRegions.at(regCounter));
                        result.stairTreads.indic.push_back(stairTreadRegions.indic[regCounter]);
                    }
                }
// stair
                for(int first = 0; first < result.stairRisers.size(); first++)
                {
                    for(int second =first+1;second<result.stairRisers.size();second++)
                    {
                        if(result.stairRisers.at(first).segmentCentroid[2] > result.stairRisers.at(second).segmentCentroid[2])
                        {
                            temp = result.stairRisers.at(first);
                            temp.segmentLabel = result.stairRisers.indic[first];
                            result.stairRisers.at(first) = result.stairRisers.at(second);
                            result.stairRisers.indic[first] = result.stairRisers.indic[second];
                            result.stairRisers.at(second) = temp;
                            result.stairRisers.indic[second] = temp.segmentLabel;
                        }
                    }
                }

                for(int first = 0; first < result.stairTreads.size(); first++)
                {
                    for(int second= first+1;second<result.stairTreads.size();second++)
                    {
                        if(result.stairTreads.at(first).segmentCentroid[2] > result.stairTreads.at(second).segmentCentroid[2])
                        {
                            temp2 = result.stairTreads.at(first);
                            temp2.segmentLabel = result.stairTreads.indic[first];
                            result.stairTreads.at(first) = result.stairTreads.at(second);
                            result.stairTreads.indic[first] = result.stairTreads.indic[second];
                            result.stairTreads.at(second) = temp2;
                            result.stairTreads.indic[second] = temp2.segmentLabel;
                        }
                    }
                }
                
                for (int i = 0; i < result.stairRisers.size()-1; i++ )
                {
                    centerSecondRiser = result.stairRisers.at(i+1).segmentCentroid.head(3);
                    centerFirstRiser = result.stairRisers.at(i).segmentCentroid.head(3);
                    if(centerSecondRiser[2] - centerFirstRiser[2] > nDistance[0] &&
                    centerSecondRiser[2] - centerFirstRiser[2] < nDistance[1] &&
                     centerSecondRiser[1] - centerFirstRiser[1] > pDistance[0] &&
                     centerSecondRiser[1] - centerFirstRiser[1] < pDistance[1])
                    {
                        stairRisers.push_back(result.stairRisers.at(i));
                        stairRisers.indic.push_back(result.stairRisers.indic[i]);
                        if ((i + 1) == result.stairRisers.size()-1)
                        {
                            stairRisers.push_back(result.stairRisers.at(i+1));
                            stairRisers.indic.push_back(result.stairRisers.indic[i+1]);
                        }
                    }
                }
                for (int i = 0; i < result.stairTreads.size()-1; i++ )
                {
                    centerSecond = result.stairTreads.at(i+1).segmentCentroid.head(3);
                    centerFirst = result.stairTreads.at(i).segmentCentroid.head(3);
                    if(centerSecond[2] - centerFirst[2] > nDistance[0] &&
                    centerSecond[2] - centerFirst[2] < nDistance[1] &&
                     centerSecond[1] - centerFirst[1] > pDistance[0] &&
                     centerSecond[1] - centerFirst[1] < pDistance[1] &&
                     centerSecond[0] - centerFirst[0] < 0.1 &&
                     centerSecond[0] - centerFirst[0] > -0.1 )
                    {
                        stairTreads.push_back(result.stairTreads.at(i));
                        stairTreads.indic.push_back(result.stairTreads.indic[i]);
                        if((i + 1) == result.stairTreads.size()-1)
                        {
                            stairTreads.push_back(result.stairTreads.at(i+1));
                            stairTreads.indic.push_back(result.stairTreads.indic[i+1]);
                        }
                    }
                }
            // ramp
               
                for( int regCounter = 0; regCounter < inclinedRegions.size(); regCounter++ )
                {
                    pcl::getMinMax3D(inclinedRegions.at(regCounter).segmentCloud,min, max);
                    incRise = fabsf(max[2]-min[2]);
                    incWidth = fabsf(inclinedRegions.at(regCounter).width);
                    incSurfAngle = inclinedRegions.at(regCounter).angle;

                    if ( -0.1 < min[2] && min[2] < 0.1 )
                    {
                        if (incRise > widthReqVec[0] && incRise < widthReqVec[1] )
                        {    
                            if( (asin(incRise/incWidth))*180/M_PI >= incSurfAngle - 10.0 && 
                            (asin(incRise/incWidth))*180/M_PI <= incSurfAngle + 10.0)
                            {
                                incSurf.push_back(inclinedRegions.at(regCounter));
                                incSurf.indic.push_back(inclinedRegions.indic[regCounter]);
                            }
                        }
                    }
                }

                for (int counter = 0; counter < segments.size(); counter++)
                {     
                    if( segments.at(counter).segmentCentroid[2] > 0.02)
                    {
                        objectRegions.push_back(segments.at(counter).segmentLabel);
                    }
                }

                if(incSurf.size()>0)
                {
                    result.rampState = true;
                    cout<<"ramp !" <<endl;
                    for (int counter = 0; counter<incSurf.size(); counter++)
                    {
                        pcl::getMinMax3D(incSurf.at(counter).segmentCloud,min, max);
                        incRise = fabsf(max[2]-min[2])*100/90;
                        result.rampDepth = (incSurf.at(counter).depth)* 100/90;
                        result.rampInc = (asin(incRise/result.rampDepth))*180/M_PI;
                        result.inclinationCloud+=incSurf.at(counter).segmentCloud;
                        result.rampPos << min[0]*100/90*100, min[1]*100/90*100; //change min[0] because the leftest one is the closest!
                    }
                    std::cout << "distance: " << result.rampPos[0] <<" " << result.rampPos[1] <<std::endl;              

                    for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
                    {
                        if(find(incSurf.indic.begin(), incSurf.indic.end(),objectRegions[regCounter]) != incSurf.indic.end())
                        {
                            objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
                            regCounter--;
                        }
                    }
                }
            
                // if (stairRisers.size() > 1|| stairTreads.size() > 1)
                if (stairTreads.size() > 1 || stairRisers.size() > 1 )
                {
                    result.stairState = true;
                    cout<<"ascending stairway detected ! " << endl;
                    if (stairRisers.size()>= stairTreads.size())
                        result.stairDistance = sqrt(pow(stairRisers.at(0).segmentCentroid[1], 2 ) + pow(stairRisers.at(0).segmentCentroid[0], 2 ))* 100 *100 /90 ;
                    else
                        result.stairDistance = sqrt(pow(stairTreads.at(0).segmentCentroid[1]-stairTreads.at(0).depth/2, 2 ) +
                                            pow(stairTreads.at(0).segmentCentroid[0], 2 ))* 100 *100/90;
                    std::cout << "distance: " << result.stairDistance <<std::endl;
                    
                    for(int counter = 0; counter < stairRisers.size(); counter ++)
                    {
                        // cout<<"step number: " << counter+1 << endl;
                        // cout<<"dimension- width: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).width*100 << " height: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).height*100 << " cm"<< endl;                        
                        // cout<<"step center- x: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).segmentCentroid[0]*100 << " y: " <<std::fixed << std::setprecision(1) <<stairRisers.at(counter).segmentCentroid[1]*100 << 
                        // " z: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).segmentCentroid[2]*100 << " cm"<< endl;
                        result.stairRiseCloud+=stairRisers.at(counter).segmentCloud;
                    }
                    for(int counter = 0; counter < stairTreads.size(); counter ++)
                    {
                        cout<<"step number: " << counter+1 << endl;
                        if(counter == 0)
                        {
                            result.stairWidth = stairTreads.at(counter).width*100* 100 /90; 
                            result.stairDepth = stairTreads.at(counter).depth*100*100/95 ;
                            result.stepCenter << stairTreads.at(counter).segmentCentroid[0]*100, 
                                            stairTreads.at(counter).segmentCentroid[1]*100, 
                                            stairTreads.at(counter).segmentCentroid[2]*100*100/95;
                        }
                        result.stairTreadCloud+=stairTreads.at(counter).segmentCloud;
                    }
                    
                    for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
                    {
                        if(find(stairTreads.indic.begin(), stairTreads.indic.end(),objectRegions[regCounter]) != stairTreads.indic.end())
                        {
                            objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
                            regCounter--;
                        }
                    }

                    for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
                    {
                        if(find(stairRisers.indic.begin(), stairRisers.indic.end(),objectRegions[regCounter]) != stairRisers.indic.end())
                        {
                            objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
                            regCounter--;
                        }
                    }

                }
                   
// object
                for(int regCounter = 0; regCounter < segments.size(); regCounter++)
                {
                    if(find(objectRegions.begin(), objectRegions.end(), segments.at(regCounter).segmentLabel) != objectRegions.end())
                    {
                        object.push_back(segments.at(regCounter));
                        object.indic.push_back(segments.at(regCounter).segmentLabel);
                    }
                }
                for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
                {
                    if(find(object.indic.begin(), object.indic.end(),objectRegions[regCounter]) != stairRisers.indic.end())
                    {
                        objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
                        regCounter--;
                    }
                }
//valid terrain
                // for(int regCounter = 0; regCounter < segments.size(); regCounter++)
                // {
                //     if(find(objectRegions.begin(), objectRegions.end(), segments.at(regCounter).segmentLabel) != objectRegions.end())
                //     {
                //         validTerrain.objectCloud+=segments.at(regCounter).segmentCloud;
                //         validTerrain.indic.push_back(segments.at(regCounter).segmentLabel);
                //     }
                // }
                // if (!floor.empty() )
                // {
                //     validTerrain.objectCloud += floor;
                // } 

                usedLabel.clear();
                objectCandidate.clear();
                for(int counter = 0; counter < object.size(); counter++)
                {
                    if(find(usedLabel.begin(), usedLabel.end(), object.indic.at(counter)) != usedLabel.end())
                    {
                        continue;
                    }
                    temp3.clear();
                    temp3.push_back(object.at(counter));
                    for(int count = counter+1; count < object.size(); count++)
                    {
                        if(find(usedLabel.begin(), usedLabel.end(), object.indic.at(count)) != usedLabel.end())
                        {
                            continue;
                        }
                        objectVector = object.at(counter).segmentCentroid.head(3) - object.at(count).segmentCentroid.head(3);
                        distanceXYObject = sqrt(pow(objectVector[0],2)+pow(objectVector[1],2));
                        distanceZObject = abs(objectVector[2]);
                        if (distanceXYObject < 0.8 )
                        {     
                            temp3.push_back(object.at(count));
                            usedLabel.push_back(object.indic.at(count));
                        }
                    }
                    if(temp3.size()>0)
                    {
                        objectCandidate.push_back(temp3); 
                    }
                }

                if(objectCandidate.size()>0)
                { 
                    Eigen::Vector4f min_pt;
                    Eigen::Vector4f max_pt;
                    float min = 10000;
                    result.objectState = true;
                    for (int objCounter = 0; objCounter < objectCandidate.size(); objCounter++)
                    {
                        PointCloudT cl = objectCandidate.at(objCounter).getObjectCloud();    
                        pcl::getMinMax3D( cl, min_pt, max_pt);
                        if (min > (min_pt[1]+max_pt[1])/2)
                        {
                            min = (min_pt[1]+max_pt[1])/2;
                            objDimension << max_pt[0]-min_pt[0], max_pt[1]-min_pt[1], max_pt[2];
                            result.objectWidth = objDimension[0] *100;
                            result.objectHeight = objDimension[2]*100;
                            result.objectPos << (min_pt[0]+max_pt[0])/2*100, min_pt[1]*100;
                        }
                    }
                }
//mode suggestion
                float stairD ;
                if (result.stairState == true) 
                {
                    stairD = result.stairDistance;
                }
                else 
                    stairD = 100000;

                float rampD;
                if (result.rampState == true) 
                {
                    rampD = sqrt(pow(result.rampPos[0],2)+pow(result.rampPos[1],2));
                
                }
                else 
                    rampD= 100000;

                float objD = sqrt(pow(result.objectPos[0],2)+pow(result.objectPos[1],2));
                std::map<float, int> mapOfModes;            
                //mapOfModes.insert(std::pair<float,int> (objD, 10));
                mapOfModes.insert(std::pair<float,int>(stairD, 4));
                mapOfModes.insert(std::pair<float,int>(rampD, 6));
                float minSuggestedDistance = 10000;
                float acceptableDistance = 200; //cm
                int estimatedMode;
                for (auto itr = mapOfModes.begin(); itr != mapOfModes.end(); ++itr)
                {
                    if(minSuggestedDistance >= itr->first && itr->first>2.0)
                    {
                        minSuggestedDistance = itr->first;
                        estimatedMode = itr->second;
                    }
                }
                if (minSuggestedDistance <= acceptableDistance)
                {
                    result.suggestedModeForSC = estimatedMode; //stop, ramp, stair
                }
                else
                {
                    result.suggestedModeForSC = 3; //straight walking
                } 

                std::cout << "suggested mode: " << result.suggestedModeForSC <<std::endl;
            } 
            
            break;
        case(false):
            {
//labeling
            for( int regCounter = 0; regCounter < stairTreadRegions.size(); regCounter++ )
            {
                Hdistance = fabsf(stairTreadRegions.at(regCounter).depth);
                if ( Hdistance > pDistance [0] && Hdistance < pDistance[1] )
                {
                    result.stairTreads.push_back(stairTreadRegions.at(regCounter));
                }
            }
//stairs
            segmentClouds firstSegment;
            stairTreads.clear();
            int base = 1;
            for (int first = 0; first < result.stairTreads.size(); first++ )
            {
                centerFirst = result.stairTreads.at(first).segmentCentroid.head(3);
                if( centerFirst[2] > -nDistance[1] && centerFirst[2] < -nDistance[0])
                    firstSegment = result.stairTreads.at(first);
                    result.stairTreadCloud += firstSegment.segmentCloud;
                    base++;
            }
            if (base != 1)
            {
                stairTreads.push_back(firstSegment);
                initialDist = firstSegment.segmentCentroid.head(3) - (firstSegment.depth / 2)*firstSegment.depth_vec;
                for(int second = 0; second < result.stairTreads.size(); second++)
                {
                    centerSecond = result.stairTreads.at(second).segmentCentroid.head(3);
                    centerThirdTreadTransform = sqrt(pow(centerSecond[0] - initialDist[0],2)+pow(centerSecond[1] - initialDist[1],2));
                    if( centerThirdTreadTransform > (base-1) * firstSegment.depth &&
                    centerThirdTreadTransform < (base) * firstSegment.depth &&
                    centerSecond[2] > -nDistance[1] * base &&
                    centerSecond[2] < -nDistance[0] * base)
                    {
                        stairTreads.push_back(result.stairTreads.at(second));
                        result.stairTreadCloud+=result.stairTreads.at(second).segmentCloud;
                        base++;
                    }
                }
            }
            for( int regCounter = 0; regCounter < inclinedRegions.size(); regCounter++ )
            {
                pcl::getMinMax3D(inclinedRegions.at(regCounter).segmentCloud,min, max);
                incRise = fabsf(max[2]-min[2]);
                incWidth = fabsf(inclinedRegions.at(regCounter).width);
                incSurfAngle = inclinedRegions.at(regCounter).angle;

                if ( -0.5 < max[2] && max[2] < 0.05 )
                {
                    if (incRise > widthReqVec[0] && incRise < widthReqVec[1] )
                    {
                        if( (asin(incRise/incWidth))*180/M_PI >= incSurfAngle - 10.0 &&
                        (asin(incRise/incWidth))*180/M_PI <= incSurfAngle + 10.0)
                        {
                            incSurf.push_back(inclinedRegions.at(regCounter));
                            incSurf.indic.push_back(inclinedRegions.indic[regCounter]);
                        }
                    }
                }
            }

            for (int counter = 0; counter < segments.size(); counter++)
            {
                if( segments.at(counter).segmentCentroid[2] < -0.02 )
                {
                    objectRegions.push_back(segments.at(counter).segmentLabel);
                }
            }
//ramp recognition
            if(incSurf.size()>0)
            {

                cout<<"ramp !" <<endl;
                result.rampState = true;
                for (int counter = 0; counter<incSurf.size(); counter++)
                {
                    pcl::getMinMax3D(incSurf.at(counter).segmentCloud,min, max);
                    incRise = fabsf(max[2]-min[2])*100;
                    result.rampDepth = (incSurf.at(counter).depth)* 100;
                    result.rampInc = (asin(incRise/result.rampDepth))*180/M_PI;
                    result.inclinationCloud+=incSurf.at(counter).segmentCloud;
                    result.rampPos << min[0]*100/90*100, max[1]*100/90*100; //change min[0] because the leftest one is the closest!
                    std::cout << "distance: " << result.rampPos[0] <<" " << result.rampPos[1] <<std::endl;              
                }

                for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
                {
                    if(find(incSurf.indic.begin(), incSurf.indic.end(),objectRegions[regCounter]) != incSurf.indic.end())
                    {
                        objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
                        regCounter--;
                    }
                }
            }
//stairs recognition
            if (stairTreads.size() > 0)
            {
                result.stairState = true;
                cout<<"descending stairway detected ! " <<endl;
                result.stairDistance = sqrt(pow(stairTreads.at(0).segmentCentroid[1]-stairTreads.at(0).depth/2, 2 ) +
                                        pow(stairTreads.at(0).segmentCentroid[0], 2 ))* 100 *100/90;
                std::cout << "distance: " << result.stairDistance <<std::endl;
                
                for(int counter = 0; counter < stairRisers.size(); counter ++)
                {
                    // cout<<"step number: " << counter+1 << endl;
                    // cout<<"dimension- width: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).width*100 << " height: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).height*100 << " cm"<< endl;
                    // cout<<"step center- x: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).segmentCentroid[0]*100 << " y: " <<std::fixed << std::setprecision(1) <<stairRisers.at(counter).segmentCentroid[1]*100 <<
                    // " z: " <<std::fixed << std::setprecision(1) << stairRisers.at(counter).segmentCentroid[2]*100 << " cm"<< endl;
                    result.stairRiseCloud+=stairRisers.at(counter).segmentCloud;
                }
                for(int counter = 0; counter < stairTreads.size(); counter ++)
                {
                    cout<<"step number: " << counter+1 << endl;
                    if(counter == 0)
                    {
                        result.stairWidth = stairTreads.at(counter).width*100* 100 /90;
                        result.stairDepth = stairTreads.at(counter).depth*100*105/85 ;
                        result.stepCenter << stairTreads.at(counter).segmentCentroid[0]*100,
                                        stairTreads.at(counter).segmentCentroid[1]*100,
                                        stairTreads.at(counter).segmentCentroid[2]*100*100/95;
                    }
                    result.stairTreadCloud+=stairTreads.at(counter).segmentCloud;
                }

                for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
                {
                    if(find(stairTreads.indic.begin(), stairTreads.indic.end(),objectRegions[regCounter]) != stairTreads.indic.end())
                    {
                        objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
                        regCounter--;
                    }
                }


            }


            // if (stairTreads.size()>0)
            // {
            //     cout<<"descending stairway detected ! " <<endl;
            //     result.stairDistance = sqrt(pow(initialDist[0],2) + pow(initialDist[1],2))* 100;
            //     cout<< "distance: "<< std::fixed << std::setprecision(1) << result.stairDistance << " cm"<< endl;

            //     for(int counter = 0; counter < stairTreads.size(); counter ++)
            //     {
            //         if(counter == 0)
            //         {
            //             result.depth = (stairTreads.at(counter).depth)* 100;
            //             result.pos << stairTreads.at(counter).segmentCentroid[0]*100 ,stairTreads.at(counter).segmentCentroid[1]*100, stairTreads.at(counter).segmentCentroid[2]*100;
            //         }
            //         cout<<"step number: " << counter+1 <<" dimension- width: " <<std::fixed << std::setprecision(1) << stairTreads.at(counter).width*100 << " depth: " <<std::fixed << std::setprecision(1) << stairTreads.at(counter).depth*100 << " cm"<< endl;
            //         cout<<"step number: " << counter+1 <<" step center- x: " <<std::fixed << std::setprecision(1) << stairTreads.at(counter).segmentCentroid[0]*100 << " y: "<<std::fixed << std::setprecision(1) << stairTreads.at(counter).segmentCentroid[1]*100
            //          << " z: "<<std::fixed << std::setprecision(1) <<stairTreads.at(counter).segmentCentroid[2]*100 << " cm"<< endl;
            //     }

            //     for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
            //     {
            //         if(find(stairTreads.indic.begin(), stairTreads.indic.end(),objectRegions[regCounter]) != stairTreads.indic.end())
            //         {
            //             objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
            //             regCounter--;
            //         }
            //     }
            // }


//object selection

            for(int regCounter = 0; regCounter < segments.size(); regCounter++)
            {
                if(find(objectRegions.begin(), objectRegions.end(), segments.at(regCounter).segmentLabel) != objectRegions.end())
                {
                    object.push_back(segments.at(regCounter));
                    object.indic.push_back(segments.at(regCounter).segmentLabel);
                }
            }

            for( int regCounter = 0; regCounter < objectRegions.size() ; regCounter++ )
            {
                if(find(object.indic.begin(), object.indic.end(),objectRegions[regCounter]) != stairRisers.indic.end())
                {
                    objectRegions.erase(objectRegions.begin()+regCounter, objectRegions.begin()+regCounter+1);
                    regCounter--;
                }
            }
//valid terrain
            // for(int regCounter = 0; regCounter < segments.size(); regCounter++)
            // {
            //     if(find(objectRegions.begin(), objectRegions.end(), segments.at(regCounter).segmentLabel) != objectRegions.end())
            //     {
            //         validTerrain.objectCloud+=segments.at(regCounter).segmentCloud;
            //         validTerrain.indic.push_back(segments.at(regCounter).segmentLabel);
            //     }
            // }
            // if (!floor.empty() )
            // {
            //     validTerrain.objectCloud += floor;
            // }
//object recognition
            usedLabel.clear();
            objectCandidate.clear();
            for(int counter = 0; counter < object.size(); counter++)
            {
                if(find(usedLabel.begin(), usedLabel.end(), object.indic.at(counter)) != usedLabel.end())
                {
                    continue;
                }
                temp3.clear();
                temp3.push_back(object.at(counter));
                for(int count = counter+1; count < object.size(); count++)
                {
                    if(find(usedLabel.begin(), usedLabel.end(), object.indic.at(count)) != usedLabel.end())
                    {
                        continue;
                    }
                    objectVector = object.at(counter).segmentCentroid.head(3) - object.at(count).segmentCentroid.head(3);
                    distanceXYObject = sqrt(pow(objectVector[0],2)+pow(objectVector[1],2));
                    distanceZObject = abs(objectVector[2]);
                    if (distanceXYObject < 0.8 )
                    {
                        temp3.push_back(object.at(count));
                        usedLabel.push_back(object.indic.at(count));
                    }
                }
                if(temp3.size()>0)
                {
                    objectCandidate.push_back(temp3);
                }
            }

            if(objectCandidate.size()>0)
            {
                Eigen::Vector4f min_pt;
                Eigen::Vector4f max_pt;
                float min = 10000;
                result.objectState = true;
                for (int objCounter = 0; objCounter < objectCandidate.size(); objCounter++)
                {
                    PointCloudT cl = objectCandidate.at(objCounter).getObjectCloud();
                    pcl::getMinMax3D( cl, min_pt, max_pt);
                    if (min > (min_pt[1]+max_pt[1])/2)
                    {
                        min = (min_pt[1]+max_pt[1])/2;
                        objDimension << max_pt[0]-min_pt[0], max_pt[1]-min_pt[1], max_pt[2];
                        result.objectWidth = objDimension[0] *100;
                        result.objectHeight = objDimension[2]*100;
                        result.objectPos << (min_pt[0]+max_pt[0])/2*100, min_pt[1]*100;
                    }
                }
            }

//mode suggestion
            float stairD ;
            if (result.stairState == true) 
            {
              stairD = result.stairDistance;
            }
            else 
                stairD = 100000;

            float rampD;
            if (result.rampState == true) 
            {
                rampD = sqrt(pow(result.rampPos[0],2)+pow(result.rampPos[1],2));
              
            }
            else 
                rampD= 100000;

            float objD = sqrt(pow(result.objectPos[0],2)+pow(result.objectPos[1],2));
            std::map<float, int> mapOfModes;
            // mapOfModes.insert(std::pair<float,int> (objD, 10));
            mapOfModes.insert(std::pair<float,int>(stairD, 5));
            mapOfModes.insert(std::pair<float,int>(rampD, 7));
            float minSuggestedDistance = 10000;
            float acceptableDistance = 400; //cm
            int estimatedMode;
            for (auto itr = mapOfModes.begin(); itr != mapOfModes.end(); ++itr)
            {
                if(minSuggestedDistance >= itr->first && itr->first>2.0)
                {
                    minSuggestedDistance = itr->first;
                    estimatedMode = itr->second;
                }
            }
            if (minSuggestedDistance <= acceptableDistance)
            {
                result.suggestedModeForSC = estimatedMode; //stop, ramp, stair
            }
            else
            {
                result.suggestedModeForSC = 3; //straight walking
            }
            std::cout << "suggested mode: " << result.suggestedModeForSC <<std::endl;
            }
            break;
    }
    output = result;
}

}
