#include <mapping_ros/library/segmentClouds.h>

namespace mapping_nodelet
{
segmentClouds::segmentClouds()
{
    segmentIndices.reset (new pcl::PointIndices);
}

//PCA 
void segmentClouds::analyse()
{
    if(segmentCloud.size() != segmentIndices->indices.size())
    {
        segmentIndices->indices.clear();
        for(int indicesAdder=0; indicesAdder<segmentCloud.size(); indicesAdder++)
            segmentIndices->indices.push_back(indicesAdder);
    }

    pcl::compute3DCentroid(segmentCloud, segmentCentroid);
    pcl::PointCloud<pcl::PointXYZ> cloudPCAprojection;
    pcl::PCA<pcl::PointXYZ> pca;
    pca.setInputCloud(segmentCloud.makeShared());
    pca.project(segmentCloud, cloudPCAprojection);
    eigen_vectors = pca.getEigenVectors() ;
    eigen_values = pca.getEigenValues() ;
    
    segmentCoefficient[0] = eigen_vectors(0,2);
    segmentCoefficient[1] = eigen_vectors(1,2);
    segmentCoefficient[2] = eigen_vectors(2,2);
    segmentCoefficient[3] = 0;
    segmentCoefficient[3] = -1 * segmentCoefficient.dot (segmentCentroid);
    
    // Get the minimum and maximum points of the transformed cloud.
    pcl::PointXYZ minPoint, maxPoint;
    pcl::getMinMax3D(cloudPCAprojection, minPoint, maxPoint);
    dimensions << maxPoint.x - minPoint.x, maxPoint.y - minPoint.y, maxPoint.z - minPoint.z;

    float eig_sum = eigen_values[0] + eigen_values[1] + eigen_values[2];
    if (eig_sum != 0)
		curvature = fabsf (eigen_values[2] / eig_sum);
    else
		curvature = 0;
}

void segmentClouds::measure()
{
    height = dimensions[1];
	width = dimensions[0];
	depth = dimensions[1];

    Eigen::Vector3f eigvec;
    eigvec << 0, 0, 1;
    
    normal=segmentCoefficient.head(3); //plane normal
    depth_vec << 0, 1, 0;
    angle = fabsf(acos(fabsf(normal.dot(eigvec)))/M_PI*180);
}

}