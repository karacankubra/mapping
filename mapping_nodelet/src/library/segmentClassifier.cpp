#include <mapping_ros/library/segmentClassifier.h>
using namespace std;

namespace mapping_nodelet
{
segmentClassifier::segmentClassifier()
{
	widthReq << 0.30, 3.0;
	treadDepth << 0.1, 0.50;
	riserHeight << 0.07, 0.25;
}
//plane segmentation
void segmentClassifier::filterSc(regions& stairTreads, regions& stairRisers, regions& inclinedSurface)
{
	stairTreads.clear();
	stairRisers.clear();
    inclinedSurface.clear();
    Eigen::Vector3f dimension;
    for(int regCounter=0; regCounter < segments.size(); regCounter++)
    {
        dimension = segments.at(regCounter).dimensions;
        if ( segments.at(regCounter).angle <= 90.0 && segments.at(regCounter).angle >= 75.0 ) //riser     
        {                
            if(segments.at(regCounter).width > 0.0 && segments.at(regCounter).width < widthReq[1])
            {
                if(segments.at(regCounter).height > riserHeight[0] && segments.at(regCounter).height < riserHeight[1])
                {
                    stairRisers.push_back(segments.at(regCounter));
                    stairRisers.indic.push_back(segments.at(regCounter).segmentLabel);
                }
            }

        }
        
        if ( segments.at(regCounter).angle <= 18.0 && segments.at(regCounter).angle >= 0.0 ) //tread
        {
            if(segments.at(regCounter).width > 0.0 && segments.at(regCounter).width < widthReq[1])
            {
                if(segments.at(regCounter).depth > treadDepth[0] && segments.at(regCounter).depth < treadDepth[1])
                {
                    if(segments.at(regCounter).segmentCentroid[2] > 0.04)
                    {
                        stairTreads.push_back(segments.at(regCounter));
                        stairTreads.indic.push_back(segments.at(regCounter).segmentLabel);
                    }
                }
            }

        }
        
        if ( segments.at(regCounter).angle < 40.0 && segments.at(regCounter).angle > 3.0 ) //15? // inclination
        {
            if(segments.at(regCounter).height> widthReq[0] && segments.at(regCounter).height < widthReq[1] )
            {
                inclinedSurface.push_back(segments.at(regCounter));
                inclinedSurface.indic.push_back(segments.at(regCounter).segmentLabel);
            }

        }

    }
}  
}