#include <mapping_ros/library/regions.h>

namespace mapping_nodelet
{
void regions::generateCenterCloud()
{
    centerCloud.clear();
    for(int regIdx = 0; regIdx < regs.size(); regIdx++)
    {
        PointT centPoint;
        centPoint.x = regs.at(regIdx).segmentCentroid[0];
        centPoint.y = regs.at(regIdx).segmentCentroid[1];
        centPoint.z = regs.at(regIdx).segmentCentroid[2];
        centerCloud.push_back(centPoint);
    }
}

PointCloudC regions::getColoredCloud ()
{

  PointCloudC colored_cloud;

  if (!regs.empty ())
  {
    srand (static_cast<unsigned int> (time (0)));
    std::vector<unsigned char> colors;
    for (size_t i_segment = 0; i_segment < regs.size (); i_segment++)
    {
      colors.push_back (static_cast<unsigned char> (rand () % 256));
      colors.push_back (static_cast<unsigned char> (rand () % 256));
      colors.push_back (static_cast<unsigned char> (rand () % 256));
    }


    int next_color = 0;
    for (size_t i_segment = 0; i_segment < regs.size (); i_segment++)
    {
      for (size_t i_point = 0; i_point < regs.at(i_segment).segmentCloud.size(); i_point++)
      {
        pcl::PointXYZRGB point;
        point.x=regs.at(i_segment).segmentCloud.at(i_point).x;
        point.y=regs.at(i_segment).segmentCloud.at(i_point).y;
        point.z=regs.at(i_segment).segmentCloud.at(i_point).z;
        point.r = colors[3 * next_color];
        point.g = colors[3 * next_color + 1];
        point.b = colors[3 * next_color + 2];
        colored_cloud.push_back(point);
      }
      next_color++;
    }
  }

  return (colored_cloud);
}


PointCloudC regions::getNormalMap ()
{
  PointCloudC colored_cloud;

  if (!regs.empty ())
  {

    for (size_t i_segment = 0; i_segment < regs.size (); i_segment++)
    {
        Eigen::Vector3i colVec;
        colVec << 255,255,255;

        colVec[0] = abs(round(255*cbrt(regs.at(i_segment).segmentCoefficient[0])));
        colVec[1] = abs(round(255*cbrt(regs.at(i_segment).segmentCoefficient[1])));
        colVec[2] = abs(round(255*cbrt(regs.at(i_segment).segmentCoefficient[2])));

      for (size_t i_point = 0; i_point < regs.at(i_segment).segmentCloud.size(); i_point++)
      {
        pcl::PointXYZRGB point;
        point.x=regs.at(i_segment).segmentCloud.at(i_point).x;
        point.y=regs.at(i_segment).segmentCloud.at(i_point).y;
        point.z=regs.at(i_segment).segmentCloud.at(i_point).z;
        point.r = colVec[0];
        point.g = colVec[1];
        point.b = colVec[2];
        colored_cloud.push_back(point);
      }
    }
  }

  return (colored_cloud);
}

PointCloudT regions::getObjectCloud ()
{
  objectCloud.clear();

  if (!regs.empty ())
  {

    for (size_t i_segment = 0; i_segment < regs.size (); i_segment++)
    {
      for (size_t i_point = 0; i_point < regs.at(i_segment).segmentCloud.size(); i_point++)
      {
        pcl::PointXYZ point;
        point.x=regs.at(i_segment).segmentCloud.at(i_point).x;
        point.y=regs.at(i_segment).segmentCloud.at(i_point).y;
        point.z=regs.at(i_segment).segmentCloud.at(i_point).z;
        objectCloud.push_back(point);
      }
    }
  }
  return (objectCloud);
}
}