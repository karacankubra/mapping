#include "mapping_ros/library/global_path.h"


namespace mapping_nodelet
{
globalPath::globalPath() // change it each time!!!!!!!!!
{
    // finalPoint[0] = -1.9/100;
    // finalPoint[1] = 69.2/100;
    // stairWidth = 129/100;
    // increment = 30;
    // small = {6, 11, 17, 22}; 
    small = {6, 11, 17, 22, 28, 33, 39, 44, 50}; 

    // mediumSmall = {13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
    // medium = {25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36};
    // mediumLarge = {37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48}; 
    // large = {49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60};
}

// std::vector<pcl::PointXYZ> globalPath::stairGlobalPath (std::vector<pcl::PointXYZ>& stepCenters)
    // {
    //     pcl::PointXYZ pts; 
    //     pcl::PointXYZ initialPt; 
    //     initialPt.x = 0; 
    //     initialPt.y = 0;
    //     initialPt.z = 0;
    //     pts.x = 0; 
    //     pts.y = 0;
    //     pts.z = 0;
    //     std::vector<pcl::PointXYZ> stairGlobPath;
    //     for (int i = 0; i < stepCenters.size(); i++)
    //     {
    //         for (int j = 0; j < increment; j++)
    //         {
    //             pts.x += (stepCenters[j].x-initialPt.x)/increment;
    //             pts.y += (stepCenters[j].y-initialPt.y)/increment;
    //             pts.z += (stepCenters[j].z-initialPt.z)/increment;
    //             stairGlobPath.push_back(pts);
    //             // std::cout <<" stair " << pts << std::endl;
    //         }
    //         initialPt.x  = stepCenters[j].x;
    //         initialPt.y  = stepCenters[j].y;
    //         initialPt.z  = stepCenters[j].z;
    //         pts.x  = stepCenters[j].x;
    //         pts.y  = stepCenters[j].y;
    //         pts.z  = stepCenters[j].z;
    //     }
    //     return stairGlobPath;
    // }




void globalPath::nominalStepLength(float &distance)
{
    smallStep(distance);
    // mediumSmallStep(distance);
    // mediumStep(distance);
    // mediumLargeStep(distance);
    // largeStep(distance);
}

void globalPath::smallStep(float &distance)
{
    int maxStepLength = 0;
    int minRem = 10000;
    float rem;
    for(int j = 0; j < small.size(); j++)
    {
       rem = ((int)distance)%small[j];
       if (minRem>=rem && maxStepLength < small[j] )
       {
            minRem = rem;
            maxStepLength = small[j];
            smallStepRequired.first = j+1;
            smallStepRequired.second = (int)(distance/small[j]);
            std::cout << "required step length: " <<  smallStepRequired.first<< std::endl;
            std::cout << "required number of steps: " << smallStepRequired.second  << std::endl;

       }
    }

   
    // std::cout<< smallStepRequired.first << " " << smallStepRequired.second << std::endl;
}

// void globalPath::mediumSmallStep(float &distance)
// {
//     int maxStepLength = 0;
//     int minRem = 10000;
//     float rem;
//     for(int j = 0; j < mediumSmall.size(); j++)
//     {
//         rem = ((int)distance)%mediumSmall[j];
//         if (minRem>=rem && maxStepLength < mediumSmall[j] )
//         {
//             minRem = rem;
//             maxStepLength = mediumSmall[j];
//             mediumSmallStepRequired.first = mediumSmall[j];
//             mediumSmallStepRequired.second = (int)(distance/mediumSmall[j]);
//         }
//     }
//     // std::cout<< mediumSmallStepRequired.first << " " << mediumSmallStepRequired.second << std::endl;
// }

// void globalPath::mediumStep(float &distance)
// {
//     int maxStepLength = 0;
//     int minRem = 10000;
//     float rem;
//     for(int j = 0; j < medium.size(); j++)
//     {
//         rem = ((int)distance)%medium[j];
//         if (minRem>=rem && maxStepLength < medium[j] )
//         {
//             minRem = rem;
//             maxStepLength = medium[j];
//             mediumStepRequired.first = medium[j];
//             mediumStepRequired.second = (int)(distance/medium[j]);
//         }
//     }
//     // std::cout<< mediumStepRequired.first << " " << mediumStepRequired.second << std::endl;
// }

// void globalPath::mediumLargeStep(float &distance)
// {
//     int maxStepLength = 0;
//     int minRem = 10000;
//     float rem;
//     for(int j = 0; j < mediumLarge.size(); j++)
//     {
//         rem = ((int)distance)%mediumLarge[j];
//         if (minRem>=rem && maxStepLength < mediumLarge[j] )
//         {
//             minRem = rem;
//             maxStepLength = mediumLarge[j];
//             mediumLargeStepRequired.first =  mediumLarge[j];
//             mediumLargeStepRequired.second = (int)(distance/ mediumLarge[j]);
//         }
//     }
// }
// void globalPath::largeStep(float &distance)
// {
//     int maxStepLength = 0;
//     int minRem = 10000;
//     float rem;
//     for(int j = 0; j < large.size(); j++)
//     {
//         rem = ((int)distance)%large[j];
//         if (minRem>=rem && maxStepLength < large[j] )
//         {
//             minRem = rem;
//             maxStepLength = large[j];
//             largeStepRequired.first = large[j];
//             largeStepRequired.second = (int)(distance/large[j]);
//         }
//     }
    
//     // std::cout<< largeStepRequired.first << " " << largeStepRequired.second << std::endl;

// }
}