#include <mapping_ros/library/solution.h>

namespace mapping_nodelet
{
solution::solution() {
	// TODO Auto-generated constructor stub
	pos << 0,0,0;
	dir << 0,0,0;
	// width = 0;
	anchPoint = 0;
	isCircular = false;
	clockwise = true;
}

//solution::~solution() {
//	// TODO Auto-generated destructor stub
//}

void solution::getColoredParts(PointCloudC& output)
{
	PointTC colPoint;
	for(int pointIdx = 0; pointIdx < stairRiseCloud.size(); pointIdx++)
	{
		colPoint.x = stairRiseCloud[pointIdx].x;
		colPoint.y = stairRiseCloud[pointIdx].y;
		colPoint.z = stairRiseCloud[pointIdx].z;
		colPoint.r=0;		
		colPoint.g=100;
		colPoint.b=100;
		output.push_back(colPoint);
	}

	for(int pointIdx = 0; pointIdx < stairTreadCloud.size(); pointIdx++)
	{
		colPoint.x = stairTreadCloud[pointIdx].x;
		colPoint.y = stairTreadCloud[pointIdx].y;
		colPoint.z = stairTreadCloud[pointIdx].z;
		colPoint.r=100;
		colPoint.g=100;
		colPoint.b=0;
		output.push_back(colPoint);
	}

	// for(int pointIdx = 0; pointIdx < stairRailCloud.size(); pointIdx++)
	// {
	// 	colPoint.x = stairRailCloud[pointIdx].x;
	// 	colPoint.y = stairRailCloud[pointIdx].y;
	// 	colPoint.z = stairRailCloud[pointIdx].z;
	// 	colPoint.r=0;
	// 	colPoint.g=255;
	// 	colPoint.b=0;
	// 	output.push_back(colPoint);
	// }

	for(int pointIdx = 0; pointIdx < inclinationCloud.size(); pointIdx++)
	{
		colPoint.x = inclinationCloud[pointIdx].x;
		colPoint.y = inclinationCloud[pointIdx].y;
		colPoint.z = inclinationCloud[pointIdx].z;
		colPoint.r=0;
		colPoint.g=255;
		colPoint.b=0;
		output.push_back(colPoint);
	}
}

// std::ostream& operator<<(std::ostream& os, solution& sc) {
// 	os << "Stair Position: "<<sc.pos[0]<<"   "<<sc.pos[1]<<"   "<<sc.pos[2]<<std::endl;
// 	os << "Stair Direction: "<<sc.dir[0]<<"   "<<sc.dir[1]<<"   "<<sc.dir[2]<<std::endl;
// 	os << "Stair Width: "<<sc.width<<std::endl;
// 	os << "Stair Anchor: "<<sc.anchPoint;
// 	return os;
// }

}