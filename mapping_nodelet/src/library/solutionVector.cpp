#include <mapping_ros/library/solutionVector.h>


namespace mapping_nodelet

{
solutionVector::solutionVector() {

}

regions solutionVector::getAllRegions()
{
	regions output;
	for(size_t vecPos = 0; vecPos < solutionVectors.size(); vecPos++)
	{
		for(size_t regionPos = 0; regionPos < solutionVectors.at(vecPos).size(); regionPos++)
		{
			output.push_back(solutionVectors.at(vecPos).at(regionPos));
		}
	}
	return output;
}

PointCloudC solutionVector::getColoredCloud(int pos)
{
	PointCloudC output;
	output += solutionVectors.at(pos).stairParts.getColoredCloud();
	return output;
}

PointCloudC solutionVector::getColoredParts(int pos)
{
	PointCloudC output;
	solutionVectors.at(pos).getColoredParts(output);
	return output;
}

void solutionVector::sort()
{
	std::sort(solutionVectors.begin(), solutionVectors.end());
}
}