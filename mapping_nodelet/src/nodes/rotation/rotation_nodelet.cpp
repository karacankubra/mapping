#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <pcl/common/time.h>
#include "mapping_ros/rotation_nodelet.h"

using namespace mapping_nodelet;

using namespace rotation;

//carry all these steps to the preparation nodelet and make a queue for theta!!
void RotationNodelet::onInit()
{
  ros::NodeHandle nh = getNodeHandle();
  sub_theta_ = nh.subscribe("/camera/imu", 1, &RotationNodelet::cbRotate, this);
  pub_theta_ = nh.advertise<geometry_msgs::TransformStamped>("/transform", 1);
}


void RotationNodelet::cbRotate(const sensor_msgs::Imu::ConstPtr& imu_)
{
    // Holds the angle as calculated from accelerometer data
    Eigen::Vector3f accel_angle;

    // Calculate rotation angle from accelerometer data
    accel_angle[2] = atan2(imu_->linear_acceleration.y, imu_->linear_acceleration.z);
    accel_angle[0] = atan2(imu_->linear_acceleration.x, sqrt(imu_->linear_acceleration.y * imu_->linear_acceleration.y + imu_->linear_acceleration.z * imu_->linear_acceleration.z));

    if (first)
    {
        first = false;
        theta << accel_angle[0],accel_angle[1], accel_angle[2];
        // Since we can't infer the angle around Y axis using accelerometer data, we'll use PI as a convetion for the initial pose
        theta[1] = M_PI;
    }
    else
    {
        /* 
        Apply Complementary Filter:
            - high-pass filter = theta * alpha:  allows short-duration signals to pass through while filtering out signals
                that are steady over time, is used to cancel out drift.
            - low-pass filter = accel * (1- alpha): lets through long term changes, filtering out short term fluctuations 
        */
        theta[0] = theta[0] * alpha + accel_angle[0] * (1 - alpha);
        theta[2] = theta[2] * alpha + accel_angle[2] * (1 - alpha);
    }
    // std::cout<< theta[0]*180/M_PI <<" " << theta[1]*180/M_PI << std::endl;
    rotationPublish(theta, imu_->header);
}

void RotationNodelet::rotationPublish(const Eigen::Vector3f &theta_, const std_msgs::Header &header )
{
  boost::shared_ptr<geometry_msgs::TransformStamped> msg = boost::make_shared<geometry_msgs::TransformStamped>();
  msg->header = header;
  msg->transform.translation.x = 0.0;
  msg->transform.translation.y = 0.0;
  msg->transform.translation.z = 0.978; 
  msg->transform.rotation.x = theta_[0];
  msg->transform.rotation.y = theta_[1];
  msg->transform.rotation.z = theta_[2];
  pub_theta_.publish(msg);
}

PLUGINLIB_EXPORT_CLASS(mapping_nodelet::rotation::RotationNodelet, nodelet::Nodelet)
