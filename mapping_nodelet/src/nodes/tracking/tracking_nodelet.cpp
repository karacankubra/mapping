/*
 * Copyright (c) 2017 Intel Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>
#include <std_msgs/Header.h>
#include "mapping_ros/tracking_nodelet.h"
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

using namespace mapping_nodelet;
using namespace tracking;

PLUGINLIB_EXPORT_CLASS(mapping_nodelet::tracking::trackingNodelet, nodelet::Nodelet);

void trackingNodelet::onInit()
{
  ros::NodeHandle nh = getNodeHandle();
  sub_obj_ = nh.subscribe("/regions", 1, &trackingNodelet::obj_cb, this);
  sub_imu_ = nh.subscribe("/camera/imu", 200 , &trackingNodelet::track_cb, this);
  pub_tracking_ = nh.advertise<mapping_msgs::TrackedObjects>("/measurements", 1);
  tm_ = boost::make_shared<Tracking>();
}

void trackingNodelet::obj_cb(const mapping_msgs::solutionConstPtr& objs)
{
    measurement_.resize(5);
    measurement_ << objs->stairCenter.x,
                    objs->stairCenter.y,
                    objs->stairCenter.z,
                    objs->stairDistance,
                    objs->stairDepth;
    measurementQueue.push(measurement_);
    state = objs->stair;
    headerMeas = objs->header;
}

void trackingNodelet::track_cb(const sensor_msgs::Imu::ConstPtr& imu_)
{

  if(!firstTime_ && state == true)
  {
    tm_->setParameter();  
    tm_->init(0,measurementQueue.front());
    firstTime_ = true;
    nextTime_ = true;
  }

  if (nextTime_ && state == true)
  {
    tm_->predict();
    if(!measurementQueue.empty())
    {
        tm_->update(measurementQueue.front());
        measurementQueue.pop();
    }
    trackingPublish( headerMeas);

  }

  if (nextTime_ && state == false)
  {
    firstTime_ = false;
    nextTime_ = false;
    trackingPublish( headerMeas);

  }
  
}

void trackingNodelet::trackingPublish(const std_msgs::Header &header)
{
  boost::shared_ptr<mapping_msgs::TrackedObjects> msg =
      boost::make_shared<mapping_msgs::TrackedObjects>();
  msg->header = header;
  tm_->getTrackedObjs(msg);
  pub_tracking_.publish(msg);
}



