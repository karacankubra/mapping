#include "mapping_ros/tracking.h"

namespace mapping_nodelet
{

namespace tracking 
{

void Tracking::setParameter ()
{
    n = 10; // Number of states
    m = 5; // Number of measurements
    dt = 1.0/500; // Time step

    A.resize(n, n); // System dynamics matrix
    C.resize(m, n); // Output matrix
    Q.resize(n, n); // Process noise covariance - normal prob. dist
    R.resize(m, m); // Measurement noise covariance - normal prob. dist
    P.resize(n, n); // Estimated error covariance - uncertainty of each moment  A <<1, 0, 0, 0, 0, dt, 0, 0, 0, 0,
    P0.resize(n, n);
    I.resize(n,n);
    x_hat.resize(n);
    x_hat_new.resize(n);
    I.setIdentity();
    A <<    1, 0, 0, 0, 0, dt, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0, dt, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, dt, 0, 0,
            0, 0, 0, 1, 0, 0, 0, 0, dt, 0,
            0, 0, 0, 0, 1, 0, 0, 0, 0, dt,
            0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 1;

    C <<    1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0;

    // Reasonable covariance matrices
    //process noise 
    Q <<.02, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, .02, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, .02, 0, 0, 0, 0, 0, 0, 0,  
        0, 0, 0, .01, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, .01, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, .002, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, .002, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, .002, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, .001, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, .001;


    //measurement noise // from sensor
    R <<    .02, 0, 0, 0, 0,
            0, .02, 0, 0, 0, 
            0, 0, .02, 0, 0, 
            0, 0, 0, .02, 0, 
            0, 0, 0, 0, .02; 
 
    //estimated error covariance // because of the prediction and measurement differemce 
    P0 <<    .01, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, .01, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, .01, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, .001, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, .01, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, .001, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, .001, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, .001, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, .0001, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, .001;

}
void Tracking::init(double t0, const Eigen::VectorXd& x0) 
{
  x_hat.setZero();
  x_hat.head(x0.size()) =  x0;
  P = P0;
  this->t0 = t0;
  t = t0;
  initialized = true;
  float disIn = (float)state()[3];
  globPath.nominalStepLength(disIn);
}

void Tracking::init() 
{
  x_hat.setZero();
  P = P0;
  t0 = 0;
  t = t0;
  initialized = true;
}

void Tracking::update(const Eigen::VectorXd &y) 
{

  if(!initialized)
    throw std::runtime_error("Filter is not initialized!");

  K = P*C.transpose()*(C*(P*C.transpose()) + R).inverse();
  x_hat_new += K * (y- C*x_hat_new);
  P = (I - K*C)*P;
  x_hat = x_hat_new;
  t += dt;
  // std::cout << " update " << " t = " << time() << " y = " <<" x_hat = " << state().transpose()[8] << std::endl; 

}


void Tracking::predict()
{
  if(!initialized)
    throw std::runtime_error("Filter is not initialized!");
  x_hat_new = A * x_hat; 
  P = A*(P*A.transpose()) + Q;
  x_hat = x_hat_new;
  // std::cout << " predict " <<" x_hat = " << prediction().transpose()/100<< std::endl;
  float disTr = (float)prediction().transpose()[3];
  globPath.nominalStepLength(disTr);
}

void Tracking::getTrackedObjs (boost::shared_ptr<mapping_msgs::TrackedObjects>& msgs)
{
    dynamic_reconfigure::ReconfigureRequest srv_req;
    dynamic_reconfigure::ReconfigureResponse srv_resp;
    dynamic_reconfigure::DoubleParameter double_param;
    dynamic_reconfigure::Config conf;

    double_param.name = "steplength";

    float StepLength = globPath.smallStepRequired.first;

    if (StepLength == 0 || StepLength < 1 || StepLength > 9)
    {
        double_param.value = 4;
    }
    else
        double_param.value = StepLength;
    conf.doubles.push_back(double_param);

    srv_req.config = conf;

    ros::service::call("/varileg_generator/set_parameters", srv_req, srv_resp);
    
    msgs->center.x = prediction().transpose()[0];
    msgs->center.y = prediction().transpose()[1];
    msgs->center.z = prediction().transpose()[2];
    msgs->distance = prediction().transpose()[3];
    msgs->depth = prediction().transpose()[4];
    msgs->speedY = prediction().transpose()[6];
    msgs->stepLength = globPath.smallStepRequired.first;
    msgs->stepNumber = globPath.smallStepRequired.second;
     if (prediction().transpose()[3]<=100.0)
        msgs->suggestedMode = 4;
     else
         msgs->suggestedMode = 3;
}

}
}
