#include <pluginlib/class_list_macros.h>
#include "mapping_ros/splitter_nodelet.h"
#include <ros/ros.h>
#include <pcl/common/time.h>

using namespace mapping_nodelet;

using namespace splitter;


void SplitterNodelet::onInit()
{
  ros::NodeHandle nh = getNodeHandle();
  sub_pc2_ = nh.subscribe("/camera/depth/color/points", 2, &SplitterNodelet::cbSplit, this);
  pub_pc_ = nh.advertise<sensor_msgs::PointCloud2>("/pointcloud", 2);
  ROS_DEBUG_STREAM("I am splitting");
  // std::cout << " I am splitting " <<std::endl;s

}

void SplitterNodelet::cbSplit(const sensor_msgs::PointCloud2::ConstPtr& points)
{
  sensor_msgs::ImagePtr image(new sensor_msgs::Image);
  Splitter::split(points, image);
  pub_pc_.publish(points);

}

PLUGINLIB_EXPORT_CLASS(mapping_nodelet::splitter::SplitterNodelet, nodelet::Nodelet)
