#include <ros/ros.h>
#include <pcl/common/time.h>
#include <pluginlib/class_list_macros.h>
#include "mapping_ros/prepare_nodelet.h"

using namespace mapping_nodelet;

using namespace prepare;

void PrepareNodelet::onInit()
{
  ros::NodeHandle nh = getNodeHandle();
  sub_ = nh.subscribe("/pointcloud", 1, &PrepareNodelet::cbSegment, this );
  sub_theta_ = nh.subscribe("/transform", 1, &PrepareNodelet::cbTheta, this );
  pub_ = nh.advertise<mapping_msgs::solution>("/regions",1);
}

void PrepareNodelet::cbTheta (const geometry_msgs::TransformStamped::ConstPtr &theta)
{
  theta_.push_back(theta);
}
void PrepareNodelet::cbSegment(const sensor_msgs::PointCloud2::ConstPtr& points)
{
  boost::shared_ptr<mapping_msgs::solution> msg = boost::make_shared<mapping_msgs::solution>();
  msg->header = points->header;

  std::vector<geometry_msgs::TransformStamped::ConstPtr>::iterator theta = theta_.begin();
  while (theta != theta_.end())
  {
    if ((*theta)->header.stamp.sec == points->header.stamp.sec)
    {
      translate_z_ = (*theta)->transform.translation.z;
      theta_z_= (*theta)->transform.rotation.x ;
      angle_z_ = (*theta)->transform.rotation.z ;
      impl_->segment(points, theta_z_, angle_z_, translate_z_, msg);
     std::cout << " theta_x  " << theta_z_*180/M_PI << std::endl;
     std::cout << " theta_z  " << angle_z_*180/M_PI << std::endl;

      pub_.publish(msg);
      theta = theta_.erase(theta);
      break;
    }
    else
    {   
      theta = theta_.erase(theta);
      continue;
    } 

  }
 
 
}

PLUGINLIB_EXPORT_CLASS(mapping_nodelet::prepare::PrepareNodelet, nodelet::Nodelet)
