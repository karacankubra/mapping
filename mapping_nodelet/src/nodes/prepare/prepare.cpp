#define PCL_NO_PRECOMPILE

#include "mapping_ros/prepare.h"
#include <memory>
#include <vector>
#include <pcl_conversions/pcl_conversions.h>
#include <mapping_nodelet/ParamsConfig.h>
using namespace mapping_nodelet;
using namespace prepare; 

void Prepare::segment(const sensor_msgs::PointCloud2::ConstPtr& points, const float &theta_z, 
                        const float &angle_z, const float &translate_z_,
                        boost::shared_ptr<mapping_msgs::solution>& msg) 
{   
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ> );
    double st = pcl::getTime();
    getPointCloud(points, cloud);
    transformCloud(cloud, theta_z, angle_z, translate_z_);
    filter (cloud);
    pcl::PointCloud<pcl::PointXYZRGB> prepNorMap;
    pcl::PointCloud<pcl::PointXYZ> floorPC;
    pcl::PointCloud<pcl::Normal>::Ptr prepNormalCloud (new pcl::PointCloud<pcl::Normal>);
    regions segRegions;    
    analyze (cloud, segRegions, floorPC);
    solution detectedStairs;
    segmentClassing(cloud,segRegions,detectedStairs, floorPC);
    composeResult (detectedStairs, msg); 
    double end2 = pcl::getTime();
    std::cout<<"running time: " << end2-st << std::endl;
}
void Prepare::getPointCloud(const sensor_msgs::PointCloud2::ConstPtr& cloud, 
                                pcl::PointCloud< pcl::PointXYZ >::Ptr& pcl_cloud) 
{
    pcl::fromROSMsg(*cloud, *pcl_cloud);          
}


void Prepare::transformCloud(pcl::PointCloud< pcl::PointXYZ >::Ptr &preCloud, const float &theta_z,
                             const float &angle_z, const float &translate_z_) 
{
    Eigen::Affine3f transform_z = Eigen::Affine3f::Identity();
    transform_z.rotate (Eigen::AngleAxisf (theta_z, Eigen::Vector3f::UnitZ()));
    transformPointCloud (*preCloud, *preCloud, transform_z);

    Eigen::Affine3f transform_IMU = Eigen::Affine3f::Identity();
    transform_IMU.rotate (Eigen::AngleAxisf (angle_z, Eigen::Vector3f::UnitX()));
    transformPointCloud (*preCloud, *preCloud, transform_IMU);
  
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    transform_2.translation() << 0.0, 0.0, translate_z_;
    transformPointCloud (*preCloud, *preCloud, transform_2);
}

void Prepare::filter(pcl::PointCloud< pcl::PointXYZ >::Ptr &cloud) 
{
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (cloud);
    pass.setFilterFieldName ("x");
    pass.setFilterLimits (-0.50, 0.50);
    pass.filter (*cloud);  
    // pass.setInputCloud (cloud);
    // pass.setFilterFieldName ("y");
    // pass.setFilterLimits (0.20, 1.50);
    // pass.filter (*cloud);  
}

void Prepare::analyze(pcl::PointCloud< pcl::PointXYZ >::Ptr &cloud, regions &segRegions, pcl::PointCloud<pcl::PointXYZ> &floorPC) 
{
    Preanalysis pre;
    pcl::PointCloud<pcl::PointXYZRGB> prepNorMap;
    pcl::PointCloud<pcl::Normal>::Ptr prepNormalCloud; 
    prepNormalCloud.reset(new pcl::PointCloud<pcl::Normal>);
    pre.run(cloud, prepNormalCloud, prepNorMap, floorPC);
    RegionGrowing reGrow;
    reGrow.setInputCloud(cloud);
    reGrow.setNormalCloud(prepNormalCloud);
    reGrow.run(segRegions);
}

void Prepare::segmentClassing(pcl::PointCloud< pcl::PointXYZ >::Ptr &cloud, const regions& segmentedRegions, 
                                        solution &detectedStairs, pcl::PointCloud<pcl::PointXYZ> &floorPC)                                    
{
    bool dir = direction(cloud);
    std::cout << " dir " << dir << std::endl; 
    segmentClassifier segClass;
    regions stairTreads;
    regions inclinedSurface;
    regions stairRisers;
    segClass.setInputRegions(segmentedRegions);
    segClass.filterSc(stairTreads, stairRisers, inclinedSurface);
    recognition objDiscovery; 
    objDiscovery.setInputRegions(segmentedRegions);
    objDiscovery.setStairTreadRegions(stairTreads);
    objDiscovery.setStairRiseRegions(stairRisers);
    objDiscovery.setInclinedRegions(inclinedSurface);
    objDiscovery.order(detectedStairs, floorPC, dir);

}

bool Prepare::direction (pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud) //added ::Ptr here
{
    Eigen::Vector4f centroid ;
    pcl::compute3DCentroid (*cloud, centroid);
    bool dir;
    if ( centroid[2] < 0 )
    {
        dir = false;
    }
    if (centroid[2]>0)
    {
        dir = true;
    }
    return dir;
}
void Prepare::composeResult (solution& detectedStairs, boost::shared_ptr<mapping_msgs::solution>& msgs )
{
    msgs->stair = detectedStairs.stairState;
    msgs->ramp = detectedStairs.rampState;
    msgs->obstacle = detectedStairs.objectState;        
    msgs->stairCenter.x = detectedStairs.stepCenter[0];
    msgs->stairCenter.y = detectedStairs.stepCenter[1];
    msgs->stairCenter.z = detectedStairs.stepCenter[2];
    msgs->stairDistance = detectedStairs.stairDistance;
    msgs->stairDepth = detectedStairs.stairDepth;
    msgs->rampPos.x = detectedStairs.rampPos[0];
    msgs->rampPos.y = detectedStairs.rampPos[1];
    msgs->obstacleCenter.x = detectedStairs.objectPos[0];
    msgs->obstacleCenter.y = detectedStairs.objectPos[1];
    msgs->obstacleHeight = detectedStairs.objectHeight;
    msgs->obstacleWidth = detectedStairs.objectWidth;
    msgs->rampInclination = detectedStairs.rampInc;
    msgs->suggestedMode = detectedStairs.suggestedModeForSC;
}

