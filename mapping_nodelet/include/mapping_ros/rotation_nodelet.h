#include <librealsense2/rs.hpp>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <ros/time.h>
#include <pluginlib/class_list_macros.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Header.h>
#include <Eigen/Dense>

#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/DoubleParameter.h>
#include <dynamic_reconfigure/Config.h>


namespace mapping_nodelet
{
namespace rotation
{

class RotationNodelet : public nodelet::Nodelet
{
 public:
  ~RotationNodelet() = default;
  Eigen::Vector3f theta;
  bool first = true;
  float alpha = 0.98;

 private:
  virtual void onInit();
  void cbRotate(const sensor_msgs::Imu::ConstPtr& imu_);

  ros::Publisher pub_theta_;
  ros::Subscriber sub_theta_;
  void rotationPublish(const Eigen::Vector3f &theta_, const std_msgs::Header &header);

};

}

}