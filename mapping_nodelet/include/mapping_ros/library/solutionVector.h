#ifndef SOLUTIONVECTOR_H_
#define SOLUTIONVECTOR_H_

#include <mapping_ros/library/solution.h>

typedef pcl::PointXYZ PointT;
typedef pcl::PointNormal PointNT;
typedef pcl::Normal Normal;
typedef pcl::PointXYZRGB PointTC;

typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointCloud<PointNT> PointCloudN;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef pcl::PointCloud<PointTC> PointCloudC;

namespace mapping_nodelet
{
class solutionVector {
public:
	solutionVector();

	std::vector<solution> solutionVectors;

	inline void push_back(solution input)
	{
		solutionVectors.push_back(input);
	}

	inline void pop_back()
	{
		solutionVectors.pop_back();
	}

	inline solution& at(int pos)
	{
		return solutionVectors.at(pos);
	}

	inline int size()
	{
		return solutionVectors.size();
	}

	inline void clear()
	{
		solutionVectors.clear();
	}

	inline void erase(int input)
	{
		solutionVectors.erase(solutionVectors.begin()+input);
	}

	void sort();

    regions getAllRegions();
    PointCloudC getColoredCloud(int pos = 0);
    PointCloudC getColoredParts(int pos = 0);
};
}
#endif /* solutionVector_H_ */
