#ifndef GLOBAL_PATH_
#define GLOBAL_PATH_

#include <iostream>
#include <vector>
#include <math.h>   
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/point_cloud.h>

//using namespace std;
namespace mapping_nodelet
{
class globalPath {
public:
    globalPath();
    // float initialPoint[2];
    // float finalPoint[2];
    // float stairWidth;
    std::vector<int> small;
    // std::vector<int> mediumSmall;
    // std::vector<int> medium;
    // std::vector<int> mediumLarge;
    // std::vector<int> large;

    std::pair<int, int> smallStepRequired;  
    // std::pair<int, int> mediumSmallStepRequired;  
    // std::pair<int, int> mediumStepRequired;  
    // std::pair<int, int> mediumLargeStepRequired;  
    // std::pair<int, int> largeStepRequired;  

    void smallStep(float& distance);
    // void mediumSmallStep(float& distance);
    // void mediumStep(float& distance);
    // void mediumLargeStep(float& distance);
    // void largeStep(float& distance);

    // pcl::PointCloud<pcl::PointXYZ>::Ptr boundedTerrain(pcl::PointCloud<pcl::PointXYZ>::Ptr& validTerrain);
    // std::vector<pcl::PointXYZ> stairGlobalPath (std::vector<pcl::PointXYZ>& stepCenters);
    void nominalStepLength(float &distance);
};

}
#endif /* GLOBAL_PATH_*/
