#ifndef PREANALYSIS_H
#define PREANALYSIS_H


#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/time.h>
#include <pcl/common/transforms.h>

#include <boost/shared_ptr.hpp>

// #include <pcl/cuda/features/normal_3d.h>
// #include <pcl/cuda/time_cpu.h>
// #include <pcl/cuda/time_gpu.h>
// #include <pcl/cuda/io/cloud_to_pcl.h>
// #include <pcl/cuda/io/extract_indices.h>
// #include <pcl/cuda/io/disparity_to_cloud.h>
// #include <pcl/cuda/io/host_device.h>
// #include <pcl/cuda/segmentation/connected_components.h>
// #include <pcl/cuda/segmentation/mssegmentation.h>

#include<iostream>

#include <mapping_ros/library/normal_3d_omp.h>

typedef pcl::PointXYZ PointT;
typedef pcl::PointNormal PointNT;
typedef pcl::Normal Normal;
typedef pcl::PointXYZRGB PointTC;

typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointCloud<PointNT> PointCloudN;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef pcl::PointCloud<PointTC> PointCloudC;

typedef std::vector<int> Indices;
namespace mapping_nodelet
{
class Preanalysis {
public:

    Preanalysis();

    void limitPC();
    void normalEstimation();
    void ghostPointFilter();
    void floorExtraction();
    void run(PointCloudT::Ptr& input, NormalCloud::Ptr& normal, PointCloudC& colMap, PointCloudT& floorPoints);

    inline void setRobotAngle(double angle)
    {
    	robAngle = angle/180*M_PI;
    }

    inline void setGpActive(bool input)
    {
        gpFlag = input;
    }



    inline
    void setNeSearch(int method)
    {
    	neNeighMethod = method;
    }

    inline
    void setSearchNeighbours(int searchNeighbors)
    {
        neSearchNeighbours = searchNeighbors;
    }

    inline
    void setSearchRadius(double searchRadius)
    {
        neSearchRadius = searchRadius;
    }

    inline
    void setGpAngle(double angle)
    {
        gpAngle = angle;
    }

    inline
    void setFsActive(bool active)
    {
        fsActive = active;
    }

    inline
    void setFsAngle(double angle)
    {
        fsAngle = angle;
    }

    inline
    void setPfActive(bool active)
    {
        pfActive = active;
    }

    inline
    void setPfAngle(double angle)
    {
        pfAngle = angle;
    }

    inline
    void setFsRange(double range)
    {
        fsRange = range;
    }


    PointCloudT::Ptr inputCloud;
    PointCloudT::Ptr pc;
    NormalCloud::Ptr normal_cloud;
    PointCloudT floorPC;
    NormalCloud floorNormal;
    Eigen::Vector4f centroid;


    double rob_x;
    double rob_y;
    double rob_z;
    double robAngle;

    double neTime;
    int neMethod;

    inline void setNeMethod(int value)
    {
    	neMethod = value;
    }

    inline void getNeTime(double& time)
    {
    	time = neTime;
    }

    bool gpFlag;

    int neNeighMethod;
    int neSearchNeighbours;
    double neSearchRadius;
    double gpAngle;
    bool fsActive;
    double fsAngle;
    double fsRange;

    bool pfActive;
    double pfAngle;

};
}
#endif // PREANALYSIS

