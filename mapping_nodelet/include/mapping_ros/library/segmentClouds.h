#ifndef SEGMENTCLOUDS_H_
#define SEGMENTCLOUDS_H_

#include <pcl/common/centroid.h>
#include <pcl/common/eigen.h>
#include <pcl/common/common.h>
#include <pcl/common/distances.h>
#include <pcl/common/transforms.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/pca.h>

#include <deque>
#include <map>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

typedef pcl::PointXYZ PointT;
typedef pcl::PointNormal PointNT;
typedef pcl::Normal Normal;
typedef pcl::PointXYZRGB PointTC;

typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointCloud<PointNT> PointCloudN;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef pcl::PointCloud<PointTC> PointCloudC;

//using namespace std;

namespace mapping_nodelet
{
class segmentClouds{
public:
	PointCloudT segmentCloud;
	NormalCloud normalCloud;
	pcl::PointIndices::Ptr segmentIndices;
	PointCloudT borderPoints;

	segmentClouds();


	~segmentClouds()
	{
		segmentCloud.clear();
		normalCloud.clear();
	}


	Eigen::Vector4f segmentCoefficient;
	Eigen::Vector4f segmentCentroid;
	Eigen::Matrix3f segmentCovariance;
   	
	Eigen::Vector3f normal;
	Eigen::Vector3f depth_vec;

	Eigen::Matrix3f eigen_vectors;
	Eigen::Vector3f eigen_values;

	Eigen::Vector3f dimensions;
	Eigen::Vector3f middleExtensions;

	float angle;
    float width;
    float height;
	float distance;
	float depth;
	float curvature;
    
	int recongitionRefs;
	int segmentLabel;
    std::vector<int> partner;
	std::vector<int> globalIndices;

	void analyse();	
	void measure();
	

};
}
#endif /* SEGMENTCloud_H_ */
