#ifndef RECOGNITION
#define RECOGNITION

#include <mapping_ros/library/regions.h>
#include <mapping_ros/library/solutionVector.h>

#include <pcl/common/centroid.h>
#include <pcl/common/eigen.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/cloud_iterator.h>
#include <pcl/common/geometry.h>
#include <pcl/common/distances.h>

#include <iostream>
#include <map>
#include <string>
#include <iterator>

typedef pcl::PointXYZ PointT;
typedef pcl::PointNormal PointNT;
typedef pcl::Normal Normal;
typedef pcl::PointXYZRGB PointTC;

typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointCloud<PointNT> PointCloudN;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef pcl::PointCloud<PointTC> PointCloudC;

typedef Eigen::Matrix<float, Eigen::Dynamic, 1> VectorX;
namespace mapping_nodelet
{
class recognition {
public:

	recognition();

    regions segments;

    bool parFlag;
    bool ndFlag;
    bool pdFlag;
    float parAngle;
    bool widthFlag;

    Eigen::Vector2f nDistance;
    Eigen::Vector2f pDistance;

    inline void setInputRegions(regions input_)
    {
        segments = input_;
        inputRegions = input_;
    }

    inline void setStairTreadRegions(regions input)
    {
        stairTreadRegions = input;
        stairTreadRegions.indic = input.indic;
    }

    inline void setStairRiseRegions(regions input)
    {
        stairRiseRegions = input;
        stairRiseRegions.indic = input.indic;
    }

    inline void setInclinedRegions(regions input)
    {
        inclinedRegions = input;
        inclinedRegions.indic = input.indic;
    }

    regions inputRegions;
    regions indicatorScRegions;
    regions indicatorSwRegions;

    regions stairTreadRegions;
    regions stairRiseRegions;
    regions inclinedRegions;

    regions tempOutput;
    regions tempComponentOut;

    regions stairTreads;
    regions stairRisers;
    regions incSurf;
    regions object;
    regions validTerrain;

    regions stairParts;

    float maxStairRiseDist;
    float maxStairRiseHDist;
    float maxStairRiseAngle;
    float maxStairTreadDist;
    float riseAngle; 
    float treadAngle; 
    Eigen::Vector3f objDimension;

    double memTime;
    double whole_time;
    double widthTime;
    double sortTime;
    double getWTime;

    bool stairRailFlag;

    int basePart;

    Eigen::Vector2f widthReqVec;

    Eigen::Vector3f distVec;
    Eigen::Vector3f startSearchPoint;
    Eigen::Vector3f stairPos;
    Eigen::Vector2i stepAmount;

    solutionVector stairs;
    int stairCount;

    std::vector<int> addedLabel;
    std::vector<int> globalAddedLabel;
    std::vector<int> objectRegions;

    bool updateFlag;
    bool optimizeFlag;

    float angleDiff;
    float distCircCent;
    bool clockWise;
    bool graphMeth;

    void order( solution& output, pcl::PointCloud<pcl::PointXYZ> &floor, bool dir );

};
}
#endif // RECOGNITION
