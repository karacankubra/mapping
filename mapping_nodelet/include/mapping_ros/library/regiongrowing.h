#ifndef REGIONGROWING
#define REGIONGROWING

#include <pcl/point_types.h>
#include <pcl/common/time.h>



#include <pcl/ModelCoefficients.h>

#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>


#include <mapping_ros/library/region_growing.h>
#include <mapping_ros/library/regions.h>

//#include <pcl/gpu/octree/octree.hpp>
//#include <pcl/gpu/containers/device_array.hpp>
//#include <mapping_ros/library/gpu_extract_clusters.h>



namespace mapping_nodelet
{
typedef pcl::PointXYZ PointT;
typedef pcl::PointNormal PointNT;
typedef pcl::Normal Normal;
typedef pcl::PointXYZRGB PointTC;

typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointCloud<PointNT> PointCloudN;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef pcl::PointCloud<PointTC> PointCloudC;

class RegionGrowing{
public:

    RegionGrowing();

    PointCloudT::Ptr inputCloud;
    NormalCloud::Ptr normalCloud;
    PointCloudC::Ptr cloud_segmented;

    int minClustSize;
    int noNeigh;
    bool smoothFlag;
    double smoothThresh;
    bool resFlag;
    double resThresh;
    bool curvFlag;
    double curvThresh;
    bool updateFlag;
    bool pointUpdateFlag;
    int updateInterval;

    inline void setPointUpdateFlag(bool value)
    {
        pointUpdateFlag = value;
    }

    inline void setClusterSize(int value)
    {
        minClustSize=value;
    }

    inline void setNoNeigh(int value)
    {
        noNeigh=value;
    }

    inline void setSmoothFlag(bool value)
    {
        smoothFlag=value;
    }

    inline void setSmoothTresh(double value)
    {
        smoothThresh=value;
    }

    inline void setResFlag(bool value)
    {
        resFlag=value;
    }

    inline void setResTresh(double value)
    {
        resThresh=value;
    }

    inline void setCurvFlag(bool value)
    {
        curvFlag=value;
    }

    inline void setCurvThresh(double value)
    {
        curvThresh = value;
    }

    inline void setUpdateFlag(bool value)
    {
        updateFlag=value;
    }
    inline void setUpdateInterval (int value)
    {
        updateInterval=value;
    }

    void run(regions& output);

    inline void setInputCloud(PointCloudT::Ptr input)
    {
        inputCloud=input;
    }
    inline void setNormalCloud(NormalCloud::Ptr normal)
    {
        normalCloud=normal;
    }

};
}
#endif // REGIONGROWING

