#ifndef solution_H_
#define solution_H_

#include <pcl/common/centroid.h>
#include <pcl/common/eigen.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <vector>

#include <mapping_ros/library/regions.h>

#include <iostream>


typedef pcl::PointXYZ PointT;
typedef pcl::PointNormal PointNT;
typedef pcl::Normal Normal;
typedef pcl::PointXYZRGB PointTC;

typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointCloud<PointNT> PointCloudN;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef pcl::PointCloud<PointTC> PointCloudC;


namespace mapping_nodelet
{
class solution {
public:
	solution();

	Eigen::Vector3f pos;
	Eigen::Vector3f dir;
	float stairDistance;
	int anchPoint;

	int stairCount;
	int stairOffset;

	bool isCircular;
	float rampDepth;
	float rampInc;
	Eigen::Vector2f rampPos;
	bool clockwise;
	float stairDepth;
	float stairWidth;
	Eigen::Vector3f stepCenter;
	Eigen::Vector2f objectPos;
	float objectWidth;
	float objectHeight;
	bool rampState;
	bool objectState;
	bool stairState;
	float height;
	float depth; 
	int suggestedModeForSC;

	float accuracy;
	Eigen::Vector3f stairScore;

	// inline Eigen::Matrix<float,7,1> coeffVec()
	// {
	// 	Eigen::Matrix<float,7,1> retMat;
	// 	retMat.head(3)=pos;
	// 	retMat[3] = width;
	// 	retMat.tail(3)=dir;
	// 	return retMat;
	// }

//	ostream& operator<<(ostream& os);

    PointCloudT stairRiseCloud;
    PointCloudT stairTreadCloud;
    PointCloudT stairRailCloud;
	PointCloudT inclinationCloud;


    regions stairParts;
    regions stairTreads;
    regions stairRisers;
	regions inclinedSurface;
    regions stairRail;
	regions restofStair;

	
    std::vector<int> planeLabels;

    void getColoredParts(PointCloudC& output);

    inline segmentClouds at(int pos)
    {
    	return stairParts.at(pos);
    }

    inline int size() const
    {
    	return stairParts.size();
    }

	friend std::ostream& operator<<(std::ostream& os, solution& sc); //able to reach private members of the function

	inline bool operator<(solution comp) const
	{
		return ((stairTreads.size() + stairRisers.size())*accuracy > (comp.stairTreads.size() + comp.stairRisers.size())*comp.accuracy);
	}
};
}
#endif /* solution_H_ */
