#ifndef REGIONS_H_
#define REGIONS_H_

#include <mapping_ros/library/segmentClouds.h>

#include <deque>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <pcl/common/time.h>
#include <pcl/filters/extract_indices.h>

typedef pcl::PointXYZ PointT;
typedef pcl::PointNormal PointNT;
typedef pcl::Normal Normal;
typedef pcl::PointXYZRGB PointTC;

typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PointCloud<PointNT> PointCloudN;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef pcl::PointCloud<PointTC> PointCloudC;

namespace mapping_nodelet
{
class regions {
public:
	std::vector<segmentClouds> regs;
	PointCloudT centerCloud;
	PointCloudT floorCloud;
	PointCloudT objectCloud;
    std::vector<int> indic;
    
    PointCloudC getColoredCloud();
    PointCloudC getNormalMap();


    void generateCenterCloud();
    PointCloudT getObjectCloud ();

	inline void combine(regions r1, regions r2)
	{
		regs = r1.regs;
		for(int curIdx = 0; curIdx < r2.size(); curIdx++)
		{
			regs.push_back(r2.at(curIdx));
		}
	}

	inline void add(regions r2)
	{
		int labelId = regs.size();
		for(int curIdx = 0; curIdx < r2.size(); curIdx++)
		{
			regs.push_back(r2.at(curIdx));
			regs.at(labelId).segmentLabel=labelId;
			labelId++;
		}
	}

    regions()
    {
    }

    ~regions()
    {
    	regs.clear();
    	centerCloud.clear();
    	floorCloud.clear();
    }

    inline void setFloorCloud(PointCloudT inputCloud)
    {
    	floorCloud = inputCloud;
    }

    inline int size() const
    {
        return regs.size();
    }

    inline void clear()
    {
        regs.clear();
        centerCloud.clear();
        floorCloud.clear();
    }

    inline segmentClouds& at(int pos)
    {
        return(regs.at(pos));
    }

    inline void push_back(segmentClouds seg)
    {
        regs.push_back(seg);
    }


    inline void getExtensions()
    {
        for(int regCount =0; regCount<regs.size(); regCount++)
        {
            regs.at(regCount).measure();
        }
    }

};

inline bool compareRefs (segmentClouds first, segmentClouds second)
{
  return (first.recongitionRefs > second.recongitionRefs);
}
}

#endif /* REGIONS_H_ */
