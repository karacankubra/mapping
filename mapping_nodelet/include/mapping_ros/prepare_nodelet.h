#define _USE_MATH_DEFINES
#include <nodelet/nodelet.h>
#include "mapping_ros/prepare.h"
#include <pluginlib/class_list_macros.h>
#include <ros/package.h>
#include <eigen3/Eigen/Geometry>
#include <geometry_msgs/TransformStamped.h>
#include <message_filters/sync_policies/approximate_time.h>

namespace mapping_nodelet 
{
namespace prepare 
{

class PrepareNodelet : public nodelet::Nodelet
{
 public:
  ~PrepareNodelet() = default;

 private:
  virtual void onInit();
  void cbTheta (const geometry_msgs::TransformStamped::ConstPtr &theta);
  void cbSegment(const sensor_msgs::PointCloud2::ConstPtr& points);

  ros::Subscriber sub_;
  ros::Subscriber sub_theta_;
  ros::Publisher pub_;
  float theta_z_;
  float angle_z_;
  float translate_z_;
  std::vector<geometry_msgs::TransformStampedConstPtr> theta_;
  std::unique_ptr<Prepare> impl_;
};
}
}