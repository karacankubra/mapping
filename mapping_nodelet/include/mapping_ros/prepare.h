#define _USE_MATH_DEFINES

#include <ros/ros.h>

#include <pcl/io/io.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <pcl/filters/passthrough.h>

#include <mapping_ros/library/preanalysis.h>
#include <mapping_ros/library/regions.h>
#include <mapping_ros/library/regiongrowing.h>
#include <mapping_ros/library/segmentClassifier.h>
#include <mapping_ros/library/recognition.h>


#include <boost/date_time/posix_time/posix_time.hpp>
#include <string>
#include <boost/bind.hpp>
#include <set>
#include <algorithm>           // std::min, std::max
#include <iostream>
#include <vector>
#include <math.h>
#include <queue>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <ros/callback_queue.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/PointCloud2.h>
#include <mapping_msgs/solution.h>
#include <pluginlib/class_list_macros.h>
#include <ros/package.h>


namespace mapping_nodelet 
{
namespace prepare
{

class Prepare
{
 public:
  Prepare() = default;
  ~Prepare() = default;
  void segment(const sensor_msgs::PointCloud2::ConstPtr& points, const float &theta_z, 
                const float &angle_z, const float &translate_z_,
                boost::shared_ptr<mapping_msgs::solution>& msg) ;
 private:
 

   void getPointCloud(const sensor_msgs::PointCloud2::ConstPtr& cloud, 
                                 pcl::PointCloud< pcl::PointXYZ >::Ptr& pcl_cloud); 

   void transformCloud(pcl::PointCloud< pcl::PointXYZ >::Ptr &preCloud, const float &theta_z, const float &angle_z, const float &translate_z_); 
   void filter(pcl::PointCloud< pcl::PointXYZ >::Ptr &cloud); 
   void analyze(pcl::PointCloud< pcl::PointXYZ >::Ptr &cloud, regions &segRegions, pcl::PointCloud<pcl::PointXYZ> &floorPC); 
   void segmentClassing(pcl::PointCloud< pcl::PointXYZ >::Ptr &cloud, const regions& segmentedRegions, 
                                          solution &detectedStairs, pcl::PointCloud<pcl::PointXYZ> &floorPC);  
   bool direction (pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud); //added ::Ptr here
   void composeResult (solution& detectedStairs, boost::shared_ptr<mapping_msgs::solution>& msgs );

};
}
}
