#include <Eigen/Dense>
#include <queue>

#include <boost/bind.hpp>
#include <set>
#include <algorithm>           // std::min, std::max
#include <iostream>
#include <vector>
#include <math.h>

#include <pcl/io/io.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <pcl/filters/passthrough.h>

#include <mapping_ros/library/preanalysis.h>
#include <mapping_ros/library/regions.h>
#include <mapping_ros/library/regiongrowing.h>
#include <mapping_ros/library/segmentClassifier.h>
#include <mapping_ros/library/recognition.h>
#include <mapping_ros/library/solutionVector.h>
#include <mapping_ros/library/global_path.h>


#include <string>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>

#include <ros/callback_queue.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/PointCloud2.h>
#include <mapping_msgs/TrackedObjects.h>
#include <mapping_msgs/solution.h>

#include <pluginlib/class_list_macros.h>
#include <ros/package.h>

#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/DoubleParameter.h>
#include <dynamic_reconfigure/Config.h>


namespace mapping_nodelet
{
namespace tracking
{
class Tracking {

public:

  /**
  * Create a Kalman filter with the specified matrices.
  *   A - System dynamics matrix
  *   C - Output matrix
  *   Q - Process noise covariance
  *   R - Measurement noise covariance
  *   P - Estimate error covariance
  */
    Tracking() = default;

  /**
  * Create a blank estimator.
  */
  ~Tracking() = default;

  /**
  * Initialize the filter with initial states as zero.
  */
  void setParameter ();

  void init();

  /**
  * Initialize the filter with a guess for initial states.
  */
  void init(double t0, const Eigen::VectorXd& x0);

  /**
  * Update the estimated state based on measured values. The
  * time step is assumed to remain constant.
  */
  void update(const Eigen::VectorXd &y);

  /**
  * Update the estimated state based on measured values,
  * using the given time step and dynamics matrix.
  */
  void predict();
  void getTrackedObjs (boost::shared_ptr<mapping_msgs::TrackedObjects>& msgs);

  /**
  * Return the current state and time.
  */
  Eigen::VectorXd state() { return x_hat; };
  Eigen::VectorXd prediction() { return x_hat_new; };
  double time() { return t; };

  // Matrices for computation
  Eigen::MatrixXd A, C, Q, R, P, K, P0;
  // System dimensions
  int m, n;

  // Initial and current time
  double t0, t;

  // Discrete time step
  double dt;

  // Is the filter initialized?
  bool initialized = false;

  // n-size identity
  Eigen::MatrixXd I;

  // Estimated states
  Eigen::VectorXd x_hat, x_hat_new;
  
  globalPath globPath;
  Eigen::Vector2d stepParameter;


};
}
}
