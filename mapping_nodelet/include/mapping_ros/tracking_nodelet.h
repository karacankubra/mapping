#define _USE_MATH_DEFINES

#include "mapping_ros/tracking.h"
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <mapping_msgs/TrackedObjects.h>
#include <mapping_msgs/solution.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Header.h>

#include <vector>
#include <queue>


namespace mapping_nodelet 
{
namespace tracking
{
class trackingNodelet : public nodelet::Nodelet
{
 public:
  
   ~trackingNodelet() = default;
    void onInit();
    bool firstTime_ = false;
    bool nextTime_ = false;
    bool state = false;
    std_msgs::Header headerMeas;
    Eigen::VectorXd measurement_;
    std::queue< Eigen::VectorXd> measurementQueue;            /**< Rgb image buffer.*/
 private:
   void obj_cb(const mapping_msgs::solutionConstPtr& objs);
   void track_cb(const sensor_msgs::Imu::ConstPtr& imu_);
   void trackingPublish(const std_msgs::Header &header);
   ros::Publisher pub_tracking_;                             /**< Tracking publisher.*/
   ros::Subscriber sub_obj_;                                 /**< Object detection subscriber.*/
   ros::Subscriber sub_imu_;                                 /**< Object detection subscriber.*/
   // std::shared_ptr<Tracking> tm_;                     /**< TrackingManager*/
   boost::shared_ptr<Tracking> tm_;
};

}
}
